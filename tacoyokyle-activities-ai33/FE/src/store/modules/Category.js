import Categories from "../../config/Category";

const state = {
  Categories: [],
};
const getters = {
  AllCategories: (state) => state.Categories,
};
const actions = {
  async fetchCategory({ commit }) {
    const response = await Categories.index();
    commit("Categories", response.data.data);
  },
};
const mutations = {
  Categories: (state, Categories) => (state.Categories = Categories),
};

export default {
  state,
  getters,
  actions,
  mutations,
};

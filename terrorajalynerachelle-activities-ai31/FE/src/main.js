import Vue from "vue";
import router from "./routes/router";
import App from "../src/App.vue";
import store from "./store/store";

import { BootstrapVue, IconsPlugin } from "bootstrap-vue";
import toastr from "toastr";
import VueFilterDateFormat from "@vuejs-community/vue-filter-date-format";
Vue.use(VueFilterDateFormat);
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap-vue/dist/bootstrap-vue.min.css";
import "./assets/css/style.css";
import axios from "axios";
axios.defaults.baseURL = "http://127.0.0.1:8000/api";
require("./store/modules/Subscriber.js");
Vue.use(BootstrapVue);
Vue.use(IconsPlugin);
Vue.config.ignoredElements = [/^ion-/];
Vue.config.productionTip = false;
Vue.use(toastr);
store.dispatch("attempt", localStorage.getItem("token")).then(() => {
  new Vue({
    store,
    router,
    render: (h) => h(App),
  }).$mount("#app");
});

<?php

namespace App\Http\Controllers;

use App\Http\Requests\CategoryRequest;
use App\Http\Controllers\CategoryValidationController;
use App\Models\categories;

class CategoryController extends Controller
{

    public function index()
    {
       $category= categories::all();
       return response()->json([
        "message" => "Categories",
        "data" => $category]);
    }

    public function create()
    {
        
    }


    public function store(CategoryRequest $request)
    {
        $category = new categories();
        $category->category = $request->category;
        $category->save();
        return response()->json($category);
    }

    public function show($id)
    {
        $category = categories::find($id);
        return response()->json($category);
    }


    public function update(CategoryRequest $request, $id)
    {
        $category = categories::findOrFail($id);
        
        $category->category = $request->category;

        $category->update();
        return response()->json($category);
    }

    public function destroy($id)
    {
        $category = categories::find($id);
        $category->delete();
        return response()->json($category);
    }
}

<?php

namespace App\Http\Controllers;


use App\Http\Requests\Returned_BooksRequest;
use App\Models\returnedBooks;


class ReturnedBook_controller extends Controller
{
    /**
     * DISPLAY
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $returnedbooks = returnedBooks::all();
        return response()->json(["message" => "List of Returned Books",
        "data" => $returnedbooks]);
        //
    }


    /**
     * SEARCH
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $returnedbooks = returnedBooks::find($id);
        return response()->json($returnedbooks);
        //
    }

}

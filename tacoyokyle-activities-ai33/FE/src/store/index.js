import Vue from "vue";
import Vuex from "vuex";
import Books from "./modules/Books";
import Patrons from "./modules/Patrons";
import Category from "./modules/Category";
import Auth from "./modules/auth";
//Create store

//load Vuex
Vue.use(Vuex);

export default new Vuex.Store({
  modules: [Books, Patrons,  Category,  Auth,],
});


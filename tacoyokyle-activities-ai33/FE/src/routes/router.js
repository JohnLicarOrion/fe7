import Vue from "vue";
import VueRouter from "vue-router";
import dashboard from "../views/pages/dashboard.vue";
import patron from "../views/pages/patron.vue";
import book from "../views/pages/book.vue";
import settings from "../views/pages/settings.vue";
import Home from "../views/Home.vue";
import Login from "../views/auth/SignIn.vue";

Vue.use(VueRouter);

let routes = new VueRouter({
  mode: "history",
  routes: [
    {
      path: "/",
      component: Login,
      meta: {
        guest: true,
      },
    },
    {
      path: "/signin",
      component: Login,
      name: "SignIn",
      meta: {
        guest: true,
      },
    },
    {
      path: "/Main",
      name: "Main",
      component: Home,
      meta: { requiresAuth: true },
      children: [
        {
          path: "/dashboard",
          name: "dashboard",
          meta: { requiresAuth: true },
          components: {
            default: Main,
            MainView: dashboard,
          },
        },
        {
          path: "/patron",
          name: "patron",
          meta: { requiresAuth: true },
          components: {
            default: Main,
            MainView: patron,
          },
        },
        {
          path: "/book",
          name: "book",
          meta: { requiresAuth: true },
          components: {
            default: Main,
            MainView: book,
          },
        },
        {
          path: "/settings",
          name: "settings",
          meta: { requiresAuth: true },
          components: {
            default: Main,
            MainView: settings,
          },
        },
      ],
    },
  ],
});

routes.beforeEach((to, from, next) => {
  if (to.matched.some((record) => record.meta.requiresAuth)) {
    if (localStorage.getItem("token") == null) {
      next({
        path: "/SignIn",
        params: { nextUrl: to.fullPath },
      });
    } else {
      next();
    }
  } else if (to.matched.some((record) => record.meta.guest)) {
    if (localStorage.getItem("token") == null) {
      next();
    } else {
      next({ name: "dashboard" });
    }
  } else {
    next();
  }
});
export default routes;

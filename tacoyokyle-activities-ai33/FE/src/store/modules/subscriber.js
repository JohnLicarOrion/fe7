import store from "@/store";
import Api from "../../config/Api";
store.subscribe((mutations) => {
  switch (mutations.type) {
    case "SET_TOKEN":
      if (mutations.payload) {
        Api.defaults.headers.common[
          "Authorization"
        ] = `Bearer ${mutations.payload}`;
        localStorage.setItem("token", mutations.payload);
      } 
      else {
        Api.defaults.headers.common["Authorization"] = null;
        localStorage.removeItem("token");
      }
      break;
  }
});

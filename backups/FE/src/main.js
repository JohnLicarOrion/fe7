import VueFilterDateFormat from "@vuejs-community/vue-filter-date-format";
import { BootstrapVue, IconsPlugin } from "bootstrap-vue";
import "bootstrap-vue/dist/bootstrap-vue.css";
import "bootstrap/dist/css/bootstrap.css";
import Vue from "vue";
import Toast from "vue-toastification";
import "vue-toastification/dist/index.css";
import Vuelidate from "vuelidate";
import App from "../src/App.vue";
import "./assets/css/bootstrap.css";
import "./assets/css/style.css";
import "./assets/css/table.css";
import router from "./routes/router";
import store from "./store/store";

Vue.use(Toast, {
  transition: "Vue-Toastification__bounce",
  maxToasts: 4,
  newestOnTop: true,
});

Vue.use(Vuelidate);
Vue.use(VueFilterDateFormat);
Vue.use(BootstrapVue);
Vue.use(IconsPlugin);

Vue.config.productionTip = false;

new Vue({
  store,
  router,
  render: (h) => h(App),
}).$mount("#app");

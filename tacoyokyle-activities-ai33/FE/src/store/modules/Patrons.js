import Patron from "../../config/Patron";

const state = {
  Patrons: [],
};

const getters = {
  AllPatrons: (state) => state.Patrons,
};

const actions = {
  async fetchPatron({ commit }) {
    const response =  await Patron.index();
    commit("Patrons", response.data.data);
  },
  async addPatron({ commit }, Patron) {
    const response =  Patron.store(Patron);
    commit("NewPatron", response.data);
  },
  async updatePatron({ commit }, Patron) {
    const response = await Patron.update(Patron);
    commit("EditPatron", response.data);
  },
  async removePatron({ commit }, Patron) {
    Patron.destroy(Patron);
    commit("DeletePatron", Patron);
  },
};

const mutations = {
  Patrons: (state, Patrons) => (state.Patrons = Patrons),
  NewPatron: (state, Patron) => state.Patrons.unshift(Patron),
  EditPatron: (state, Patron) => {
    const index = state.Patrons.findIndex((t) => t.id === Patron.id);
    if (index !== -1) {
      state.Patrons.splice(index, 1, Patron);
    }
  },
  DeletePatron: (state, Patron) =>
    (state.Patrons = state.Patrons.filter((t) => Patron.id !== t.id)),
};

export default {
  state,
  getters,
  actions,
  mutations,
};

import Api from "./Api";
const Base = "auth/dashboard";
export default {
  dashboard() {
    return Api.get(Base);
  },
};

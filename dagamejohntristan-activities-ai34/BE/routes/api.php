<?php

use App\Http\Controllers\AuthController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\BookController;
use App\Http\Controllers\PatronController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('auth')
    ->group(function () {
        Route::post('login', [AuthController::class, 'login'])->name('login');
        Route::post('logout',  [AuthController::class, 'logout']);
        Route::get('dashboard', [AuthController::class, 'me'])->middleware('auth:api');
    });

Route::group(['middleware' => ['auth:api']], function () {
    Route::Resources([
        'categories' => CategoryController::class,
        'book' => BookController::class,
        'patron' => PatronController::class
    ]);
});

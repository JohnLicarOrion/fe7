<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BookRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'name' => 'requred|max:255',
            'author' => 'requred|max:100',
            'category_id' => 'requred|max:100',
            'copies' => 'requred|integer|max:100'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Name is required.',
            'name.max' => 'Name must maximum of 255 characters.',
            'author.required' => 'Author is required.',
            'author.max' => 'Author must maximum of 255 characters.',
            'copies.required' => 'Copies are requred',
            'copies.integer' => 'Use Int!',
            'copies.gt' => 'Copies must greater than 0.',
        ];
    }
}

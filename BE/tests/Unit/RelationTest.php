<?php

namespace Tests\Unit;

use App\Models\Books;
use App\Models\Borrowed_Books;
use App\Models\Categories;
use App\Models\Patrons;
use App\Models\Returned_Books;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Builder;

class RelationTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_a_category_has_many_books()

    {

        $category = Categories::create(['category' => 'Computer Science']); 

        Books::create([
            'name' => 'Cleant Architecture', 
            'author' => 'Robert Martin',
            'category_id' => $category->id,
            'copies' => rand(1, 5) 
        ]);

        $this->assertInstanceOf(Collection::class, $category->books);

    }

    public function test_a_book_if_it_belongs_to_a_category()
    {

        $category = Categories::create(['category' => 'Computer Science']); 
        $book = Books::create([
            'name' => 'Cleant Architecture', 
            'author' => 'Robert Martin',
            'category_id' => $category->id,
            'copies' => rand(1, 5) 
        ]);

        $this->assertInstanceOf(Categories::class, $book->category);
    }

    public function test_a_book_if_it_has_many_borrowed()
    {
        $category = Categories::create(['category' => 'Computer Science']);
        $patron = Patrons::create([
            'last_name' => 'Orion',
            'first_name' => 'John Licar',
            'middle_name' => 'Orion', 
            'email' => 'licar242@gmail.com'

        ]);

        $book = Books::create([
            'name' => 'Cleant Architecture', 
            'author' => 'Robert Martin',
            'category_id' => $category->id,
            'copies' => rand(1, 5) 
        ]);

        Borrowed_Books::create([
            'patron_id' => $patron->id,
            'copies' => rand(1, 5),
            'book_id' => $book->id 

        ]);

        $this->assertInstanceOf(Builder::class, Books::with('borrowed'));
    }

    public function test_a_book_if_it_has_many_returned()
    {
        $category = Categories::create(['category' => 'Computer Science']);
        $patron = Patrons::create([
            'last_name' => 'Orion',
            'first_name' => 'John Licar',
            'middle_name' => 'Orion', 
            'email' => 'licar242@gmail.com'

        ]);

        $book = Books::create([
            'name' => 'Cleant Architecture', 
            'author' => 'Robert Martin',
            'category_id' => $category->id,
            'copies' => rand(1, 5) 
        ]);

        Returned_Books::create([
            'patron_id' => $patron->id,
            'copies' => rand(1, 5),
            'book_id' => $book->id 

        ]);

        $this->assertInstanceOf(Builder::class, Books::with('returned'));
    }

    public function test_patron_if_it_has_many_borrowed_book()
    {
        $category = Categories::create(['category' => 'Computer Science']);
        $patron = Patrons::create([
            'last_name' => 'Orion',
            'first_name' => 'John Licar',
            'middle_name' => 'Orion', 
            'email' => 'licar242@gmail.com'

        ]);

        $book = Books::create([
            'name' => 'Cleant Architecture', 
            'author' => 'Robert Martin',
            'category_id' => $category->id,
            'copies' => rand(1, 5) 
        ]);

        Borrowed_Books::create([
            'patron_id' => $patron->id,
            'copies' => rand(1, 5),
            'book_id' => $book->id 

        ]);

        $this->assertInstanceOf(Builder::class, Patrons::with('borrowed'));
    }

    public function test_patron_if_it_has_many_returned_book()
    {
        $category = Categories::create(['category' => 'Computer Science']);
        $patron = Patrons::create([
            'last_name' => 'Orion',
            'first_name' => 'John Licar',
            'middle_name' => 'Orion', 
            'email' => 'licar242@gmail.com'

        ]);

        $book = Books::create([
            'name' => 'Cleant Architecture', 
            'author' => 'Robert Martin',
            'category_id' => $category->id,
            'copies' => rand(1, 5) 
        ]);

        Returned_Books::create([
            'patron_id' => $patron->id,
            'copies' => rand(1, 5),
            'book_id' => $book->id 

        ]);

        $this->assertInstanceOf(Builder::class, Patrons::with('return'));
    }

    
    public function test_borrowed_book_if_it_belongs_to_a_book()
    {
        $category = Categories::create(['category' => 'Computer Science']);
        $patron = Patrons::create([
            'last_name' => 'Orion',
            'first_name' => 'John Licar',
            'middle_name' => 'Orion', 
            'email' => 'licar242@gmail.com'

        ]);

        $book = Books::create([
            'name' => 'Cleant Architecture', 
            'author' => 'Robert Martin',
            'category_id' => $category->id,
            'copies' => rand(1, 5) 
        ]);

        Borrowed_Books::create([
            'patron_id' => $patron->id,
            'copies' => rand(1, 5),
            'book_id' => $book->id 

        ]);

        $this->assertInstanceOf(Builder::class, Borrowed_Books::with('book'));
    }

    public function test_borrowed_book_if_it_belongs_to_a_patron()
    {
        $category = Categories::create(['category' => 'Computer Science']);
        $patron = Patrons::create([
            'last_name' => 'Orion',
            'first_name' => 'John Licar',
            'middle_name' => 'Orion', 
            'email' => 'licar242@gmail.com'

        ]);

        $book = Books::create([
            'name' => 'Cleant Architecture', 
            'author' => 'Robert Martin',
            'category_id' => $category->id,
            'copies' => rand(1, 5) 
        ]);

        Borrowed_Books::create([
            'patron_id' => $patron->id,
            'copies' => rand(1, 5),
            'book_id' => $book->id 

        ]);

        $this->assertInstanceOf(Builder::class, Borrowed_Books::with('patron'));
    }

    
    public function test_returned_book_if_it_belongs_to_a_book()
    {
        $category = Categories::create(['category' => 'Computer Science']);
        $patron = Patrons::create([
            'last_name' => 'Orion',
            'first_name' => 'John Licar',
            'middle_name' => 'Orion', 
            'email' => 'licar242@gmail.com'

        ]);

        $book = Books::create([
            'name' => 'Cleant Architecture', 
            'author' => 'Robert Martin',
            'category_id' => $category->id,
            'copies' => rand(1, 5) 
        ]);

        Returned_Books::create([
            'patron_id' => $patron->id,
            'copies' => rand(1, 5),
            'book_id' => $book->id 

        ]);

        $this->assertInstanceOf(Builder::class, Borrowed_Books::with('book'));
    }

    public function test_returned_book_if_it_belongs_to_a_patron()
    {
        $category = Categories::create(['category' => 'Computer Science']);
        $patron = Patrons::create([
            'last_name' => 'Orion',
            'first_name' => 'John Licar',
            'middle_name' => 'Orion', 
            'email' => 'licar242@gmail.com'

        ]);

        $book = Books::create([
            'name' => 'Cleant Architecture', 
            'author' => 'Robert Martin',
            'category_id' => $category->id,
            'copies' => rand(1, 5) 
        ]);

        Returned_Books::create([
            'patron_id' => $patron->id,
            'copies' => rand(1, 5),
            'book_id' => $book->id 

        ]);

        $this->assertInstanceOf(Builder::class, Borrowed_Books::with('patron'));
    }
}

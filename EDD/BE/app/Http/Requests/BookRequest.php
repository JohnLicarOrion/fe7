<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BookRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'max:100|required',
            'author' => 'max:100|required',
            'copies' => 'integer|max:100|required',
            'category_id' => 'integer|required'
        ];
    }
}

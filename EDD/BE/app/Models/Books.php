<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Books extends Model
{
    use HasFactory;
    protected $fillable = ['name', 'author', 'copies', 'catgeory_id'];

    public function category()
    {
        return $this->belongsTo(categories::class,'category_id');
    }
    public function borrowed()
    {
        return $this->hasMany(borrowed_books::class);
    }
    public function returned()
    {
        return $this->hasMany(returned_books::class);
    }
}

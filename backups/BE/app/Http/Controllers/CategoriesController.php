<?php

namespace App\Http\Controllers;

use App\Models\Categories;
use Illuminate\Http\Request;
use App\Http\Requests\CategoriesRequest;

class CategoriesController extends Controller
{
    /**
     * DISPLAY
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Categories::all();
        return response()->json([
            "message" => "List of Categories",
            "data" => $categories]);
    }


    /**
     * ADD
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoriesRequest $request)
    {
        $categories = new Categories();
        $categories->category = $request->category;
        $categories->save($request->validated());
        return response()->json($categories);
    }


    /**
     * SEARCH
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $categories = Categories::find($id);
        return response()->json($categories);
    }


    /**
     * UPDATE
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CategoriesRequest $request, $id)
    {
        $categories = Categories::find($id);
        
        $categories->category = $request->category;

        $categories->update($request->validated());
        return response()->json($categories);
    }

    
    /**
     * DELETE
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $categories = Categories::find($id);
        $categories->delete();
        return response()->json($categories);
    }
}
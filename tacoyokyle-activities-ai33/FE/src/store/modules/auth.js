import Dashboard from "@/config/DashboardURI";
import SignIn from "@/config/SignInURI";
import SignOut from "@/config/SignOutURI";
export default {
  state: {
    token: "",
    user: {},
  },
  getters: {
    authenticated(state) {
      return state.token && state.user;
    },
    user(state) {
      return state.user;
    },
  },

  actions: {
    async signIn({ dispatch }, User) {
      let response = await SignIn.login(User);
      return dispatch("attempt", response.data.token);
      
    },

    async attempt({ commit, state }, token) {
      if (token) commit("SET_TOKEN", token);
      if (!state.token) return;

      try {
        let response = await Dashboard.dashboard();

        commit("SET_USER", response);

       
      } catch (error) {
        commit("SET_TOKEN", null);
        commit("SET_USER", {});
      }
    },

    signOut({ commit }) {
      return SignOut.logout().then(() => {
        commit("SET_TOKEN", null);
        commit("SET_USER", {});
      });
    },
  },
  mutations: {
    SET_TOKEN: (state, token) => (state.token = token),
    SET_USER: (state, data) => (state.user = data),
  },
};

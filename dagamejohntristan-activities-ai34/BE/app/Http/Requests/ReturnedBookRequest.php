<?php

namespace App\Http\Requests;

use App\Models\BorrowedBook;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Foundation\Http\FormRequest;

class ReturnedBookRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $borrowed = BorrowedBook::where([
            ['book_id', request()->get('book_id')], 
            ['patron_id', request()->get('patron_id')]
        ])->first();

        if (!empty($borrowed)) {

            $copies = $borrowed->copies;

        } else {

            $copies = request()->get('copies');
        } 

        return [
            'book_id' => 'bail|required|exists:borrowed_books,book_id',
            'copies' => ['gt:0', "lte: {$copies}", 'required', 'bail'],
            'patron_id' => 'exists:borrowed_books,patron_id'
        ];
    }

    public function messages()
    {
        return [
            'book_id.exists' => 'The book is not found in the borrowed books',
            'copies.lte' => 'Copies exceeded the total copies of book',
            'patron_id.exists' => 'The patron is not found in the borrowed books'
        ];
    }

}

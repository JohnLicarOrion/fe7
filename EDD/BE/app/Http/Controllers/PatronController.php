<?php

namespace App\Http\Controllers;

use App\Models\patrons;
use App\Http\Requests\PatronRequest;

class PatronController extends Controller
{

    public function index()
    {
        $patron = patrons::all();
        return response()->json([
            "message"=>"Patrons",
            "data"=>$patron
        ]);
    }
    
    public function create()
    {
        
    }


    public function store(PatronRequest $request)
    {
        $patron = new patrons();

        $patron->last_name = $request->last_name;
        $patron->first_name = $request->first_name;
        $patron->middle_name = $request->middle_name;
        $patron->email = $request->email;

        $patron->save();
        return response()->json($patron);
    }


    public function show($id)
    {
        $patron= patrons::find($id);
        return response()->json($patron);
    }


    public function update(PatronRequest $request, $id)
    {
        $patron= patrons::find($id);
        $patron->last_name = $request->input('last_name');
        $patron->first_name = $request->input('first_name');
        $patron->middle_name = $request->input('middle_name');
        $patron->email = $request->input('email');
        $patron->update();
        return response()->json($patron);
    }

    public function destroy($id)
    {
        $patron = patrons::find($id);
        $patron->delete();
        return response()->json($patron);
    }
}

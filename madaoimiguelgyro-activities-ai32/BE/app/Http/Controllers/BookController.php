<?php

namespace App\Http\Controllers;

use App\Models\Book;
use App\Http\Requests\BookRequest;
use Illuminate\Http\Request;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $books = Book::all();
        return response()->json([
            "message" => "Books",
            "data" => $books]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BookRequest $request)
    {
        $books = new Book();

        $books->name = $request ->name;
        $books->author = $request ->author;
        $books->copies = $request ->copies;
        $books->category_id = $request ->category_id;

        $validated = $request->validated();

        $books->save();
        return response()->json($books);
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BookRequest $request, $id)
    {
        $books = Book::find($id);

        $books->name = $request->input('name');
        $books->author = $request->input('author');
        $books->copies = $request->input('copies');
        $books->category_id = $request->input('category_id');
        
        $validated = $request->validated();

        $books->update();
        return response()->json($books);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $books = Book::find($id);
        $books->delete();
        return response()->json($books);
    }
}

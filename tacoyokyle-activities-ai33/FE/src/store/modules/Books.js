import Book from "../../config/Book";

const state = {
  Books: [],
};

const getters = {
  AllBooks: (state) => state.Books,
};

const actions = {
  async fetchBooks({ commit }) {
    const response = await Book.index();
    commit("Books", response.data.data);
  },
  async addBooks({ commit }, Books) {
    const response = Book.store(Books);
    commit("NewBooks", response.data.data);
  },
  async updateBooks({ commit }, Books) {
    const response = await Book.update(Books);
    commit("EditBooks", response.data);
  },
  async removeBooks({ commit }, Books) {
    Book.destroy(Books);
    commit("DeleteBooks", Books);
  },
};

const mutations = {
  Books: (state, Books) => (state.Books = Books),
  NewBooks: (state, Books) => state.Books.unshift(Books),
  EditBooks: (state, Books) => {
    const index = state.Books.findIndex((t) => t.id === Books.id);
    if (index !== -1) {
      state.Books.splice(index, 1, Books);
    }
  },
  DeleteBooks: (state, Books) =>
    (state.Books = state.Books.filter((t) => Books.id !== t.id)),
};

export default {
  state,
  getters,
  actions,
  mutations,
};

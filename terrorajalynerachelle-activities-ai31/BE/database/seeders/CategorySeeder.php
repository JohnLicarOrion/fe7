<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;


class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    
    $mul_rows= [
    [ 'category' => 'Contemporary'],
    [ 'category' => 'Fantasy'],
    [ 'category' => 'Mystery'],
    [ 'category' => 'Romance'],
    [ 'category' => 'Thriller'],
    [ 'category' => 'History'],
    [ 'category' => 'Science Fiction']
];

    foreach ($mul_rows as $rows) {
    Category::create($rows);
        }
    }
}

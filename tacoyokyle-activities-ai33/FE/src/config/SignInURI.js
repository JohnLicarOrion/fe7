import Api from "./Api";
const Base = "auth/login";
export default {
  login(User) {
    return Api.post(Base, User);
  },
};

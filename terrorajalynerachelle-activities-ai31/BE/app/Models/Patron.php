<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Patron extends Model
{
    use HasFactory;


    protected $table = 'patrons';
    protected $fillable = ['last_name', 'first_name', 'middle_name', 'email'];

    public function BorrowedBook(){
        return $this->hasMany(BorrowedBook::class, 'borrowed_books',  'patron_id');
    }

    public function ReturnedBook(){
        return $this->hasMany(ReturnedBook::class, 'returned_books', 'patron_id');
    }
}

import VueFilterDateFormat from "@vuejs-community/vue-filter-date-format";
import "bootstrap";
import { BootstrapVue, IconsPlugin } from "bootstrap-vue";
import "bootstrap-vue/dist/bootstrap-vue.min.css";
import "bootstrap/dist/css/bootstrap.min.css";
import Vue from "vue";
import Toast from "vue-toastification";
import "vue-toastification/dist/index.css";
import App from "../src/App.vue";
import router from "./router/router";
import store from "./store/store";
import axios from "axios";
require("./store/modules/Subscriber.js");
axios.defaults.baseURL = "http://127.0.0.1:8000/api";
Vue.use(Toast, {
  transition: "Vue-Toastification__bounce",
  maxToasts: 3,
  newestOnTop: true,
});

Vue.use(VueFilterDateFormat);
Vue.use(BootstrapVue);
Vue.use(IconsPlugin);
Vue.config.productionTip = false;
store.dispatch("attempt", localStorage.getItem("token")).then(() => {
  new Vue({
    store,
    router,
    render: (h) => h(App),
  }).$mount("#app");
});

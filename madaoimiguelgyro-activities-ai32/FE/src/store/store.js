import Vue from "vue";
import Vuex from "vuex";
import Books from "./modules/Books";
import Categories from "./modules/Category";
import Patrons from "./modules/Patrons";
import Auth from "./modules/auth";

//Create store

//load Vuex
Vue.use(Vuex);

export default new Vuex.Store({
  modules: [Auth, Books, Patrons, Categories],
});

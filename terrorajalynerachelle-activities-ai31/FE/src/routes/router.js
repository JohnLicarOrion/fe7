import Vue from "vue";
import VueRouter from "vue-router";
import dashboard from "../pages/dashboard.vue";
import patron from "../pages/patron.vue";
import book from "../pages/book.vue";
import setting from "../pages/setting.vue";
import SignIn from "../pages/auth.vue";
import Main from "../pages/Home.vue";
Vue.use(VueRouter);

let routes = new VueRouter({
  mode: "history",
  routes: [
    {
      path: "/",
      component: SignIn,
      meta: { guest: true },
    },
    {
      path: "/SignIn",
      name: "SignIn",
      component: SignIn,
      meta: { guest: true },
    },
    {
      path: "/Main",
      name: "Main",
      component: Main,
      meta: { requiresAuth: true },
      children: [
        {
          path: "/",
          component: dashboard,
          meta: {
            requiresAuth: true,
          },
        },
        {
          path: "/dashboard",
          name: "Dashboard",
          component: dashboard,
          meta: {
            requiresAuth: true,
          },
        },
        {
          path: "/patron",
          name: "Patron",
          component: patron,
          meta: {
            requiresAuth: true,
          },
        },

        {
          path: "/book",
          name: "Book",
          component: book,
          meta: {
            requiresAuth: true,
          },
        },

        {
          path: "/setting",
          name: "Settings",
          component: setting,
          meta: {
            requiresAuth: true,
          },
        },
      ],
    },
  ],
});

routes.beforeEach((to, from, next) => {
  if (to.matched.some((record) => record.meta.requiresAuth)) {
    if (localStorage.getItem("token") == null) {
      next({
        path: "/SignIn",
        params: { nextUrl: to.fullPath },
      });
    } else {
      next();
    }
  } else if (to.matched.some((record) => record.meta.guest)) {
    if (localStorage.getItem("token") == null) {
      next();
    } else {
      next({ name: "Dashboard" });
    }
  } else {
    next();
  }
});
export default routes;

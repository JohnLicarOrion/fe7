import Vue from "vue";
import VueRouter from "vue-router";
import Dashboard from "../view/dashboard.vue";
import Patron from "../view/patronmng.vue";
import Book from "../view/bookmng.vue";
import Settings from "../view/setting.vue";

Vue.use(VueRouter);

export default new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      component: Dashboard,
    },
    {
      path: "/Dashboard",
      name: "Dashboard",
      component: Dashboard,
    },

    {
      path: "/Book",
      name: "Book",
      component: Book,
    },
    {
      path: "/Patron",
      name: "Patron",
      component: Patron,
    },
    {
      path: "/Settings",
      name: "Settings",
      component: Settings,
    },
  ],
});

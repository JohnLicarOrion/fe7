<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BorrowedBookRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'patron_id' => 'integer|required',
            'copies' => 'integer|max:100|required',
            'book_id' => 'integer|required'
        ];
    }
}

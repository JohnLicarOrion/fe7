<?php

namespace App\Http\Controllers;
use App\Models\borrowedBooks;
use App\Models\returnedBooks;
use App\Models\Book;
use App\Http\Requests\Borrowed_BooksRequest;

class BorrowedBook_controller extends Controller
{
   public function index()
    {
        $borrowedbooks = borrowedBooks::all();
        return response()->json(["message" => "List of Borrowed Books",
        "data" => $borrowedbooks]);
    }



    /**
     * ADD
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Borrowed_BooksRequest $request)
    {
        $borrowedbooks = new borrowedBooks();
        $borrowedbooks->copies = $request->copies;
        $borrowedbooks->book_id = $request->book_id;
        $borrowedbooks->patron_id = $request->patron_id;
        $books = Book::find($borrowedbooks->book_id);
        $subtractCopies = $books->copies - $borrowedbooks->copies;
        $borrowedbooks->save($request->validated());
        $books->update(['copies' => $subtractCopies]);
        return response()->json(
            ["message" => "Success",
            ["message" => "Borrowed Successfully",
            "data" => $borrowedbooks, $books]
            ]
        );
    }


    /**
     * SEARCH
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id){
        $BookBorrowd = borrowedBooks::find($id);
        return response()->json($BookBorrowd);
    }



    /**
     * UPDATE
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Borrowed_BooksRequest $request, $id)
    {
        $borrowedbook = borrowedBooks::find($id);
        $returnedbook = new returnedBooks();
        $returnedbook->copies = $request->copies;
        $returnedbook->book_id = $request->book_id;
        $returnedbook->patron_id = $request->patron_id;
        $books = Book::find($returnedbook->book_id);
        $addCopies = $books->copies + $returnedbook->copies;
        $returnedbook->save();
        $borrowedbook->delete();
        $returnedbook->save($request->validated());
        $borrowedbook->delete($request->validated());
        $books->update(['copies' => $addCopies]);
        return response()->json(["message" => "Success",
        "data" => $borrowedbook, $books, $returnedbook]);
    }
}

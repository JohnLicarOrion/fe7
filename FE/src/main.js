import VueFilterDateFormat from "@vuejs-community/vue-filter-date-format";
import { BootstrapVue, IconsPlugin } from "bootstrap-vue";
import "bootstrap-vue/dist/bootstrap-vue.min.css";
import "bootstrap/dist/css/bootstrap.min.css";
import Vue from "vue";
// import Toasted from "vue-toasted";
import Toast from "vue-toastification";
import "vue-toastification/dist/index.css";
import App from "../src/App.vue";
import router from "./routes/router";
import store from "./store/store";

Vue.use(Toast, {
  transition: "Vue-Toastification__bounce",
  maxToasts: 3,
  newestOnTop: true,
});

Vue.use(VueFilterDateFormat);
Vue.use(BootstrapVue);
Vue.use(IconsPlugin);
Vue.config.productionTip = false;

// Vue.use(Toasted, {
//   duration: 3000,

// });

new Vue({
  store,
  router,
  render: (h) => h(App),
}).$mount("#app");

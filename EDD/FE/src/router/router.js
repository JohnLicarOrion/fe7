import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../pages/Main.vue";
import Login from "../pages/login.vue";
import Dashboard from "../pages/dashboard.vue";
import Patron from "../pages/patron.vue";
import Book from "../pages/book.vue";
import Settings from "../pages/setting.vue";

Vue.use(VueRouter);

let router = new VueRouter({
  mode: "history",
  routes: [
    {
      path: "/",
      component: Login,
      meta: {
        guest: true,
      },
    },
    {
      path: "/login",
      component: Login,
      name: "SignIn",
      meta: {
        guest: true,
      },
    },

    {
      path: "/Main",
      name: "Main",
      component: Home,
      meta: { requiresAuth: true },
      children: [
        {
          path: "/Dashboard",
          component: Dashboard,
          name: "Dashboard",
          meta: {
            requiresAuth: true,
          },
        },

        {
          path: "/Book",
          name: "Book",
          component: Book,
          meta: {
            requiresAuth: true,
          },
        },
        {
          path: "/Patron",
          name: "Patron",
          component: Patron,
          meta: {
            requiresAuth: true,
          },
        },
        {
          path: "/Settings",
          name: "Settings",
          component: Settings,
          meta: {
            requiresAuth: true,
          },
        },
      ],
    },
  ],
});
router.beforeEach((to, from, next) => {
  if (to.matched.some((record) => record.meta.requiresAuth)) {
    if (localStorage.getItem("token") == null) {
      next({
        path: "/login",
        params: { nextUrl: to.fullPath },
      });
    } else {
      next();
    }
  } else if (to.matched.some((record) => record.meta.guest)) {
    if (localStorage.getItem("token") == null) {
      next();
    } else {
      next({ name: "Dashboard" });
    }
  } else {
    next();
  }
});
export default router;

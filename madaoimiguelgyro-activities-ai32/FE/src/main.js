import Vue from 'vue'
import router from "./routes/router";
import App from "../src/App.vue";
import { BootstrapVue, IconsPlugin } from "bootstrap-vue";
import toastr from "toastr";
import store from "./store/store"
import axios from "axios";
import VueFilterDateFormat from "@vuejs-community/vue-filter-date-format";
require("./store/modules/subscriber.js");
axios.defaults.baseURL = 'http://127.0.0.1:8000/api';
Vue.use(VueFilterDateFormat);
Vue.use(BootstrapVue);
Vue.use(IconsPlugin);




import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap-vue/dist/bootstrap-vue.min.css";
import './assets/css/design.css'

Vue.config.productionTip = false;
Vue.use(toastr);
store.dispatch("attempt", localStorage.getItem("token")).then(() => {
  new Vue({
    router,
    store,
    render: (h) => h(App),
  }).$mount("#app");
});

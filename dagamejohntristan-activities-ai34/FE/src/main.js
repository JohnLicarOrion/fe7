import Vue from "vue";
import { BootstrapVue, IconsPlugin } from "bootstrap-vue";
import VueFilterDateFormat from "vuelidate";

import router from "./routes/router";
import store from "./store/store";
import App from "../src/App.vue";
import Toast from "vue-toastification";

Vue.use(BootstrapVue);
Vue.use(IconsPlugin);
Vue.config.productionTip = false;

import "./assets/css/style.css";
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";
import "bootstrap-vue/dist/bootstrap-vue.min.css";
import axios from "axios";
require("./store/modules/Subscriber.js");
Vue.use(VueFilterDateFormat);
Vue.use(Toast, {
  transition: "Vue-Toastification__bounce",
  maxToasts: 3,
  newestOnTop: true,
});
axios.defaults.baseURL = "http://127.0.0.1:8000/api";
Vue.config.productionTip = false;
store.dispatch("attempt", localStorage.getItem("token")).then(() => {
  new Vue({
    store,
    router,
    render: function(h) {
      return h(App);
    },
  }).$mount("#app");
});

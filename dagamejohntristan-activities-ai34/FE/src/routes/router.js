import Vue from "vue";
import VueRouter from "vue-router";
import Dashboard from "../components/pages/index/Dashboard.vue";
import Patron from "../components/pages/patron/Patron.vue";
import Book from "../components/pages/books/Books.vue";
import Settings from "../components/pages/settings/Settings.vue";
import SignIn from "../components/pages/auth/auth.vue";
import Main from "../components/pages/Home.vue";

Vue.use(VueRouter);

let routes = new VueRouter({
  mode: "history",
  routes: [
    {
      path: "/",
      component: SignIn,
      meta: { guest: true },
    },
    {
      path: "/SignIn",
      name: "SignIn",
      component: SignIn,
      meta: { guest: true },
    },
    {
      path: "/Main",
      name: "Main",
      component: Main,
      meta: { requiresAuth: true },
      children: [
        {
          path: "/Dashboard",
          name: "Dashboard",
          component: Dashboard,
          meta: {
            requiresAuth: true,
          },
        },
        {
          path: "/Patron",
          name: "Patron",
          component: Patron,
          meta: {
            requiresAuth: true,
          },
        },
        {
          path: "/Book",
          name: "Book",
          component: Book,
          meta: {
            requiresAuth: true,
          },
        },

        {
          path: "/Settings",
          name: "Settings",
          component: Settings,
          meta: {
            requiresAuth: true,
          },
        },
      ],
    },
  ],
});

routes.beforeEach((to, from, next) => {
  if (to.matched.some((record) => record.meta.requiresAuth)) {
    if (localStorage.getItem("token") == null) {
      next({
        path: "/SignIn",
        params: { nextUrl: to.fullPath },
      });
    } else {
      next();
    }
  } else if (to.matched.some((record) => record.meta.guest)) {
    if (localStorage.getItem("token") == null) {
      next();
    } else {
      next({ name: "Dashboard" });
    }
  } else {
    next();
  }
});
export default routes;

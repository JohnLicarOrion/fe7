import Vue from "vue";
import Vuex from "vuex";
import Books from "./modules/Books";
import Patrons from "./modules/Patron";
import Auth from "./modules/Authentication";
//Create store

//load Vuex
Vue.use(Vuex);

export default new Vuex.Store({
  modules: [Books, Patrons, Auth],
});

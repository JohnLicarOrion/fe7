<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\PatronRequest;
use App\Models\Patron;
use Illuminate\Http\Request;

class PatronController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $patrons = Patron::all();
        return response()->json([
            "message" => "Patron List",
            "data" => $patrons]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $patrons = new Patron();

        $patrons->last_name = $request->last_name;
        $patrons->first_name = $request->first_name;
        $patrons->middle_name = $request->middle_name;
        $patrons->email = $request->email;

        $patrons->save();
        return response()->json($patrons);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $patrons = Patron::find($id);
        return response()->json($patrons);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $patrons = Patron::find($id);
        
        $patrons->last_name = $request->input('last_name');
        $patrons->first_name = $request->input('first_name');
        $patrons->middle_name = $request->input('middle_name');
        $patrons->email = $request->input('email');

        $patrons->update();
        return response()->json($patrons);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $patrons = Patron::find($id);
        $patrons->delete();
        return response()->json($patrons);
    }
}

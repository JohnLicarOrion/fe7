<?php

namespace App\Http\Controllers;

use App\Models\BorrowedBook;
use Illuminate\Http\Request;
use App\Http\Requests\BorrowedBookRequest;

class BorrowedBookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(BorrowedBook::with([
            'patron', 'book', 'categories'
        ])->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $create_borrowed = BorrowedBook::create($request->only(['book_id', 'copies', 'patron_id']));
        $borrowed_book = BorrowedBook::with(['book'])->find($create_borrowed->id);
        $copies = $borrowed_book->book->copies - $resquest->copies;
        $borrowed_book->book->update(['copies' => $copies]);

        return response()->json(['message' => 'Borrowed Successfully', 'borrowed_book' => $borrowed_book]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $borrowed_book = BorrowedBook::with(['patron', 'book', 'categories'])->where('id', $id)->firstOrFail();
        return response()->json($borrowed_book);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

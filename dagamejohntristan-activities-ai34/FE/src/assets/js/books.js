export default {
    data() {
   return {
     Form: {
       BookName: "",
       Author: "",
       CopiesAvailable: "",
       Category: "",
       BooksDateRegistered: new Date(),
     },
     BooksModel: [],
     SelectedBooks: "",
     Search: "",
   };
 },
 methods: {
   AddBooks() {
     this.BooksModel.unshift(this.Form);
     console.log(this.form);
     this.HideModal();
   },
   UpdateBooks(index) {
     console.log(index);
     this.BooksModel.splice(index, 1, this.Form);

     this.HideModal();
   },
   ToBeUpdated(index) {
     this.SelectedBooks = index;
     this.Form.BookName = this.BooksModel[index].BookName;
     this.Form.Author = this.BooksModel[index].Author;
     this.Form.CopiesAvailable = this.BooksModel[index].CopiesAvailable;
     this.Form.Category = this.BooksModel[index].Category;
   },
   DeleteBooks(index) {
     this.BooksModel.splice(index, 1);

     this.HideModal();
   },
   HideModal() {
     this.$refs.modal.hide();
     this.FormReset();
   },
   FormReset() {
     this.Form = {
       BookName: "",
       Author: "",
       CopiesAvailable: "",
       Category: "",
       BooksDateRegistered: new Date(),
     };

     this.SelectedBooks = "";
   },
 },
 computed: {
   BooksModelFilter() {
     return this.BooksModel.filter((Books) => {
       return this.Search.toLowerCase()
         .split(" ")
         .every((v) => Books.BookName.toLowerCase().includes(v));
     });
   },
 },
};

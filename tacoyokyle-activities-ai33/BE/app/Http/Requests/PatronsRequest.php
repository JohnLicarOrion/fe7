<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PatronsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'Last_name' => 'max:50|required',
            'First_name' => 'max:50|required',
            'Middle_name' => 'max:50|required',
            'Email' => 'required|email'
            //
        ];
    }
}

<?php

use App\Http\Controllers\Auth\DashboardtController;
use App\Http\Controllers\Auth\SigninController;
use App\Http\Controllers\Auth\SignoutController;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'auth', 'namespace' => 'Auth'], function () {
    Route::post('login', 'SigninController')->name('login');
    Route::post('logout', 'SignoutController');
    Route::get('dashboard', 'DashboardtController')->middleware(['auth:api']);
});


Route::middleware(['auth:api'])
    ->group(function () {
        Route::resource('Categories', 'CategoryController');
        Route::resource('Books', "Book_controller");
        Route::resource('Patrons', "Patron_controller");
        Route::resource('Borrowed_Book', 'BorrowedBook_controller');
        Route::resource('Returned_Books', 'ReturnedBook_controller');
    });

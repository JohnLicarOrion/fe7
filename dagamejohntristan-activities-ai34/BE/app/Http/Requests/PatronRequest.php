<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Foundation\Http\FormRequest;

class PatronRequest extends FormRequest
{
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'requred|max:100',
            'middle_name' => 'requred|max:100',
            'last_name' => 'requred|max:100',
            'email' => 'requred|email:rfc,dns'
        ];
    }

    public function messages()
    {
        return [
            'last_name.required' => 'Input Lastname',
            'last_name.min' => 'error',
            'last_name.max' => 'Input Firstname',
            'last_name.required' => 'input Last Name.',
            'last_name.min' => 'Last name must minimum of 3 characters.',
            'last_name.max' => 'Last name must maximum of 50 characters.',
            'middle_name.required' => 'Input Middle name',
            'middle_name.min' => 'Middle name must minimum of 3 characters.',
            'middle_name.max' => 'Middle name must maximum of 50 characters.',
            'email.required' => 'Input Email.',
            'email.email' => 'Email is not valid .',
            'email.unique' => 'Email already exist.'
        ];
    }
}

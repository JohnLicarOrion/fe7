import Vue from "vue";
import VueRouter from "vue-router";
import Dashboard from "../components/pages/Dashboard.vue";
import Patron from "../components/pages/Patron.vue";
import Book from "../components/pages/Books.vue";
import Settings from "../components/pages/Settings.vue";
import SignIn from "../components/auth/SignIn"
import Home from "../components/Home"

Vue.use(VueRouter);

let router = new VueRouter({
  mode: "history",
  routes: [

    {
      path: "/",
      component: SignIn,
      meta: { guest: true },
    },
    {
      path: "/signin",
      name: "signin",
      component: SignIn,
      meta: { guest: true },
    },
    {
      path: "/Main",
      name: "Main",
      component: Home,
      meta: { requiresAuth: true },
      children: [
          {
            path: "/Dashboard",
            name: "Dashboard",
            meta: { requiresAuth: true },
            components: {
              default: Home,
              MainView: Dashboard,
            }
          },
          {
            path: "/Patron",
            name: "Patron",
            meta: { requiresAuth: true },
            components: {
              default: Home,
              MainView: Patron,
            }
          },

        {
            path: "/Book",
            name: "Book",
            meta: { requiresAuth: true },
            components: {
              default: Home,
              MainView: Book,
            }
          },
          
          {
            path: "/Settings",
            name: "Settings",
            meta: { requiresAuth: true },
            components: {
              default: Home,
              MainView: Settings,
            }
          },
      ],
      
    }
    
  ],

});
router.beforeEach((to, from, next) => {
  if(to.matched.some(record => record.meta.requiresAuth)) {
      if (localStorage.getItem('token') == null) {
          next({
              path: '/signin',
              params: { nextUrl: to.fullPath }
          })
      } else {       
              next()
      }
  } else if(to.matched.some(record => record.meta.guest)) {
      if(localStorage.getItem('token') == null){
          next()
      }
      else{
          next({ name: 'Dashboard'})
      }
  }else {
      next()
  }
})
export default router
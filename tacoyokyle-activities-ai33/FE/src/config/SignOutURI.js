import Api from "./Api";
const Base = "auth/logout";
export default {
  logout() {
    return Api.post(Base);
  },
};

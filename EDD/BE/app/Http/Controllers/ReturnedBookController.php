<?php

namespace App\Http\Controllers;

use App\Http\Requests\ReturnedBookRequest;
use App\Models\returned_books;

class ReturnedBookController extends Controller
{

    public function index()
    {
        $returned_book = returned_books::all();
        return response()->json([
            "message" => "List of Returned Books",
            "data" => $returned_book
        ]);
    }

 
    public function show($id)
    {
        $returned_book = returned_books::find($id);
        return response()->json($returned_book);
    }

}

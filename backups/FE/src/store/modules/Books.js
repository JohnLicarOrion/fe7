import axios from "axios";

const state = {
  books: [],
};

const getters = {
  AllBooks: (state) => state.books,
};

const actions = {
  async fetchBook({ commit }) {
    const response = await axios.get("http://127.0.0.1:8000/api/books");
    commit("Books", response.data);
  },

  async addBook({ commit }, Book) {
    const response = await axios.post(
      "http://127.0.0.1:8000/api/books",
      Book
    );
    commit("NewBook", response.data.data);
  },

  async updateBook({ commit }, Book) {
    const response = await axios.put(
      `http://127.0.0.1:8000/api/books/${Book.id}`,
      Book
    );
    commit("EditBook", response.data);
  },

  async deleteBook({ commit }, Book) {
    axios.delete(`http://127.0.0.1:8000/api/books/${Book.id}`, Book);
    commit("DeleteBook", Book);
  },
};

const mutations = {
  Books: (state, Books) => (state.books = Books),
  NewBook: (state, Book) => state.books.unshift(Book),
  EditBook: (state, Book) => {
    const index = state.books.findIndex((t) => t.id === Book.id);
    if (index !== -1) {
      state.books.splice(index, 1, Book);
    }
  },
  DeleteBook: (state, Book) =>
    (state.books = state.books.filter((t) => Book.id !== t.id)),
};

export default {
  state,
  getters,
  actions,
  mutations,
};

<?php

namespace App\Http\Requests;

use App\Models\BorrowedBook;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Foundation\Http\FormRequest;

class ReturnedBookRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $borrowed = BorrowedBook::where([
            ['book_id', request()->get('book_id')], 
            ['patron_id', request()->get('patron_id')]
        ])->first();
        
        if (!empty($borrowed)) {
            
            $copies = $borrowed->copies;
            
        } else {
        
            $copies = request()->get('copies');
        } 

        return [
            'copies' => ['gt:0', "lte: {$copies}", 'required', 'bail'],
            'book_id' => 'bail|required|exists:borrowed_books,book_id',
            'patron_id' => 'exists:borrowed_books,patron_id'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'book_id.exists' => 'The book cannot be found in the borrowed books.',
            'copies.lte' => 'The borrowed copies given exceeded the total copies of borrowed books.',
            'patron_id.exists' => 'The patron cannot be found in the borrowed books.'
        ];
    }

    public function failedValidation(Validator $validator)
    {

        throw new HttpResponseException(response()->json($validator->errors(), 422));
    }

}

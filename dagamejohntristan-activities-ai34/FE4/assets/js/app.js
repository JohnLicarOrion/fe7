var ctx = document.getElementById('barChart').getContext('2d');
new Chart(ctx, {
type: 'bar',
data: {
labels: ['JAN', 'FEB', 'MARCH', 'APRIL', 'MAY', 'JUNE'],
datasets: [
      {
        label: 'Borrowed Books',
        data: [7, 10, 9, 12, 13, 6],
        backgroundColor: 'rgba(119, 122, 233, .9)',
      },
      {
label: 'Returned Books',
data: [56, 8, 20, 11, 16, 15],
backgroundColor: 'rgba(154, 179, 245, .8)',
      },
    ],
  },
  options: {
  scales: {
  yAxes: [{
  ticks: {
  beginAtZero: true
        }
      }]
    }
  }
});

var pie = document.getElementById('pieChart').getContext('2d');
new Chart(pie, {
type: 'doughnut',
data: {
datasets: [
      {
data: [90, 10, 20, 150],
backgroundColor: [
          'rgb(52, 152, 219, .8)',
          'rgba(154, 179, 245, .8)',
          'rgba(27, 206, 230, .8)',
          'rgba(27, 64, 230, .8)',
        ],
      }
    ],

labels: [
      'Novel',
      'Fiction',
      'Biography',
      'Horror',
    ]
  },
options: {
scales: {
yAxes: [{
ticks: {
beginAtZero: true
        }
      }]
    }
  }
});


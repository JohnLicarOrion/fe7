import Vue from "vue";
import Vuex from "vuex";
import Books from "./modules/Book";
import Categories from "./modules/Category";
import Patrons from "./modules/Patrons";
//Create store

//load Vuex
Vue.use(Vuex);

export default new Vuex.Store({
  modules: [Books, Patrons, Categories],
});

export default {
    data() {
   return {
     Form: {
       bookname: "",
       author: "",
       copiesAvailable: "",
       category: "",
       BookDateRegistered: new Date(),
     },
     BookModel: [],
     SelectedBook: "",
     Search: "",
   };
 },
 methods: {
   AddBook() {
     this.BookModel.unshift(this.Form);
     console.log(this.form);
     this.HideModal();
   },
   UpdateBook(index) {
     console.log(index);
     this.BookModel.splice(index, 1, this.Form);

     this.HideModal();
   },
   ToBeUpdated(index) {
     this.SelectedBook = index;
     this.Form.bookname = this.BookModel[index].bookname;
     this.Form.author = this.BookModel[index].author;
     this.Form.copiesAvailable = this.BookModel[index].copiesAvailable;
     this.Form.category = this.BookModel[index].category;
   },
   DeleteBook(index) {
     this.BookModel.splice(index, 1);

     this.HideModal();
   },
   HideModal() {
     this.$refs.modal.hide();
     this.FormReset();
   },
   FormReset() {
     this.Form = {
       bookname: "",
       author: "",
       copiesAvailable: "",
       category: "",
       BookDateRegistered: new Date(),
     };

     this.SelectedBook = "";
   },
 },
 computed: {
   BookModelFilter() {
     return this.BookModel.filter((Book) => {
       return this.Search.toLowerCase()
         .split(" ")
         .every((v) => Book.bookname.toLowerCase().includes(v));
     });
   },
 },
};
